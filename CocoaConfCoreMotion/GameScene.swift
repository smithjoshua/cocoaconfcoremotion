//
//  GameScene.swift
//  CocoaConfCoreMotion
//
//  Created by Josh Smith on 3/21/16.
//  Copyright (c) 2016 Josh Smith. All rights reserved.
//

import SpriteKit
import CoreMotion
import GameplayKit

class Accel : GKState {}
class Gyro: GKState {}
class CMMM: GKState {}

let state = GKStateMachine(states: [Accel(), Gyro(), CMMM()])

class GameScene: SKScene {
    
    let cmmanger = CMMotionManager()
    let pedometer = CMPedometer()
    let activitymanager = CMMotionActivityManager()
    
    let privateQueue = NSOperationQueue()
    
    override func didMoveToView(view: SKView) {
        cmmanger.startAccelerometerUpdates()
        cmmanger.startGyroUpdates()
        cmmanger.startMagnetometerUpdates()
        self.physicsWorld.gravity = CGVector.zero
        
        state.enterState(CMMM)
        
        cmmanger.startDeviceMotionUpdates()
        if CMPedometer.isStepCountingAvailable() {
            pedometer.startPedometerUpdatesFromDate(NSDate()) { pedometer_data, error  in
                let steps = self.childNodeWithName("steps_hud") as! SKLabelNode
                if let step_count = pedometer_data?.numberOfSteps {
                    steps.text = "\(step_count)"
                }
            }
        } else {
            let steps = self.childNodeWithName("steps_hud") as! SKLabelNode
            steps.text = "- None -"
        }
        
        let date_formattr = NSDateFormatter()
        date_formattr.timeStyle = .ShortStyle

        if CMMotionActivityManager.isActivityAvailable() {
            activitymanager.startActivityUpdatesToQueue(privateQueue) { handler in
                if let handler_actual = handler {
                    let activty_hud = self.childNodeWithName("activity_hud") as! SKLabelNode
                    let activity_start_time = self.childNodeWithName("activity_start_time") as! SKLabelNode
                    if handler_actual.running {
                        activty_hud.text = "Running"
                    }
                    
                    if handler_actual.walking {
                        activty_hud.text = "Walking"
                    }
                    
                    if handler_actual.stationary {
                        activty_hud.text = "Stationary"
                    }
                    
                    if handler_actual.automotive {
                        activty_hud.text = "Auto"
                    }
                    
                    if handler_actual.unknown {
                        activty_hud.text = "Unknown"
                    }
                    
                    if handler_actual.cycling {
                        activty_hud.text = "Cycling"
                    }
                    activity_start_time.text = date_formattr.stringFromDate(handler_actual.startDate)
                }
            }
        } else {
            let activty_hud = self.childNodeWithName("activity_hud") as! SKLabelNode
            let activity_start_time = self.childNodeWithName("activity_start_time") as! SKLabelNode
            activty_hud.text = " - None - "
            activity_start_time.text = " - None - "
        }
    }
    
    override func touchesEnded(touches: Set<UITouch>, withEvent event: UIEvent?) {
        if let touch = touches.first {
            if let ship = self.childNodeWithName("ship") {
                ship.position = touch.locationInNode(self)
                ship.physicsBody?.velocity = CGVectorMake(0, 0)
            }
        }
    }
   
    override func update(currentTime: CFTimeInterval) {

        let smallship = self.childNodeWithName("rotation_ship") as! SKSpriteNode
        switch state.currentState {
            case is Accel:
                useAccelerometer(smallship)
            case is Gyro:
                useGyro(smallship)
            case is CMMM:
                useMotion(smallship)
            default:
            break
        }

        if let magnets = cmmanger.magnetometerData?.magneticField {
            self.backgroundColor = SKColor(red: CGFloat(abs(magnets.x)),
                    green: CGFloat(abs(magnets.y)),
                    blue: CGFloat(abs(magnets.z)), alpha: 1.0)
            }
    }
    
    func useAccelerometer(ship: SKSpriteNode) {
         if let gravity_vector_acceleration = cmmanger.accelerometerData?.acceleration {
            let d_x = CGFloat(gravity_vector_acceleration.x)
            let d_y = CGFloat(gravity_vector_acceleration.y)
            self.physicsWorld.gravity = CGVectorMake(d_x, d_y)
            ship.zRotation = CGFloat(gravity_vector_acceleration.z)
        }
    }
    
    func useGyro(ship: SKSpriteNode) {
        if let gyro_vector = cmmanger.gyroData?.rotationRate {
            let d_x = CGFloat(gyro_vector.x)
            let d_y = CGFloat(gyro_vector.y)
            self.physicsWorld.gravity = CGVectorMake(d_x, d_y)
            ship.zRotation = CGFloat(gyro_vector.z)
        }
    }
    
    func useMotion(ship: SKSpriteNode) {
        if let device_motion = cmmanger.deviceMotion {
            self.physicsWorld.gravity = CGVectorMake(CGFloat(device_motion.gravity.x), CGFloat(device_motion.gravity.y))
            let roll = device_motion.attitude.roll
            ship.zRotation = CGFloat(roll)
        }
    }
}
